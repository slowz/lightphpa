lightphpa
==========
LAMP

Apache Mysql PHP Installer for Debian 8

Low memory usage

First I'd like to say this script installs extra software some people might find useless.

Some of the extra software installed
(dnsutils tmux nano wget curl grc xtail htop iftop vim-nox grc xtail apache2
php5 php5-cgi php5-gd mc iotop zip unzip php-apc php5-curl php5-gd
php5-intl php5-mcrypt php-gettext php5-mysql php5-sqlite php5-cli php-pear sqlite3 php5-imagick)

Installs and configures ufw
open: 80,sshport,443
out going open

Secures sysctl.conf

Change ssh port

Option for ssh keys only ssh login

FPM runing as domain user

Sets limits and secure php

~~Remove syslogd (memory hog) replaced by inetutils-syslogd~~

Setup logwatch with offsite email

Mysql default storage myisam

Tested on Debian 8

Installs Apache2.4, MySql and PHP5 from debian repos.
Option for installing webmin from repos.
Setting up user and virutal host in /home/user/domain.com
Random mysql root password in /root/.my.cnf and mysql user for the added domain.
PHP few setting for security and performance.


Install: No install. just run the script



**Run bash lighttphpa.sh as root**

### Quick Install (Git)

    # Install git and clone Lightphpa
    apt-get install git -y
    git clone https://gitlab.com/slowz/lightphpa.git
    cd lightphpa

**You must set the options in options.conf**

`bash lightphpa.sh install`

`bash lightphpa.sh adddomain` ## adds /home/user/domain/public_html and adds info to lighttpd

`bash lightphpa.sh version` ## Show version
